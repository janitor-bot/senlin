senlin (14.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 11:09:28 +0200

senlin (14.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:42:16 +0200

senlin (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Sep 2022 09:52:10 +0200

senlin (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:37:43 +0200

senlin (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 09:10:11 +0200

senlin (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.
  * Removed fix-import-from-collections.patch.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 19:32:24 +0100

senlin (12.0.0-2) unstable; urgency=medium

  * Add install-missing-files.patch.
  * Add fix-import-from-collections.patch (Closes: #1002384).

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Dec 2021 23:54:19 +0100

senlin (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 21:53:39 +0200

senlin (12.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 15:41:41 +0200

senlin (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 21:38:15 +0200

senlin (11.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 19:18:20 +0200

senlin (11.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 17:52:13 +0200

senlin (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 27 Mar 2021 23:46:25 +0100

senlin (10.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2020 20:54:24 +0200

senlin (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Update config generator namespace list.
  * Switch to yaml policy file in /etc/senlin/policy.d.
  * Add a conductor and health-manager package.

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Sep 2020 22:42:43 +0200

senlin (9.0.0-1) unstable; urgency=medium

  * Fixed Homepage URL.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 19:18:48 +0200

senlin (9.0.0~rc2-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 17:27:31 +0200

senlin (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 25 Apr 2020 00:22:37 +0200

senlin (8.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Oct 2019 22:35:22 +0200

senlin (8.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 19:22:21 +0200

senlin (8.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2019 09:36:47 +0200

senlin (7.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 17 Jul 2019 19:51:11 +0200

senlin (7.0.0-2) experimental; urgency=medium

  * d/control:
      - Bump openstack-pkg-tools to version 99
      - Add me to uploaders field
  * d/copyright: Add me to copyright file

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 02 May 2019 18:36:57 +0200

senlin (7.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 11:09:51 +0200

senlin (7.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 4.3.0 (no change).
  * Removed package versions when satisfied in Buster.
  * Add oslo.report and oslo.upgradecheck as (build-)depends.
  * Removed python3- prefix when calling config generators.
  * Blacklist 2 unit tests:
    - TestNodePollUrlHealthCheck.test_run_health_check_unhealthy
    - TestNodePollUrlHealthCheck.test_run_health_check_conn_error

 -- Thomas Goirand <zigo@debian.org>  Sat, 30 Mar 2019 11:33:04 +0100

senlin (6.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Run all tests.
  * Remove /etc/senlin/{api-paste.ini,policy.json} on purge (Closes: #905509).
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 19:54:22 +0200

senlin (6.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Dropped Fix_unit_tests_to_make_py36_happy.patch applied upstream.
  * Blacklist engine.test_node.TestNode.test_run_workflow that has an ordering
    problem.
  * Blacklist obviously wrong test, failing in Python 3.7 because the error
    message is different there:
    - OpenStackSDKTest.test_parse_exception_http_exception_no_details
  * d/rules: rm -rf $(CURDIR)/debian/tmp/usr/etc

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Aug 2018 22:59:24 +0200

senlin (5.0.1-2.1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * Fixed dbc postrm.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Mar 2018 18:01:04 +0000

senlin (5.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Switched to openstack-pkg-tools >= 70~ shared debconf templates.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Mar 2018 10:26:29 +0000

senlin (5.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https in Format
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.
  * Switch to Python 3.
  * Also package examples and contrib folder in senlin-doc.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Feb 2018 15:13:18 +0000

senlin (4.0.0-4) unstable; urgency=medium

  [ David Rabel ]
  * Team upload.
  * Fix encoding in d/po/pt.po.

 -- Thomas Goirand <zigo@debian.org>  Sat, 10 Feb 2018 12:49:11 +0000

senlin (4.0.0-3) unstable; urgency=medium

  * Uploading to unstable.
  * Added pt_BR.po (Closes: #861983).
  * Updated fr.po (Closes: #873469).
  * Updated pt.po (Closes: #876176).
  * Fixed minimum version of python-docker (Closes: #880127).

 -- Thomas Goirand <zigo@debian.org>  Tue, 31 Oct 2017 10:36:50 +0000

senlin (4.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Updated VCS fields.
  * Ran wrap-and-sort -bast.
  * Drop sqlalchemy version patch.
  * Updating maintainer field.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.1.
  * Fixed (build-)depends for this release.
  * debian/rules: remove things defined by openstack-pkg-tools.
  * Fixed oslo-config-generator namespace list.
  * Remove dh-systemd build-depends (not needed with debhelper 10).

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Oct 2017 16:30:25 +0000

senlin (2.0.0-3) unstable; urgency=medium

  * Team upload.
  * Bump build dependency on openstack-pkg-tools (Closes: #858712).

 -- David Rabel <david.rabel@noresoft.com>  Sat, 01 Apr 2017 12:35:16 +0200

senlin (2.0.0-2) unstable; urgency=medium

  * Team upload.
  * Bumped debhelper compat version to 10
  * Added lsb-base to depends
  * Patch-out upper constraints of SQLAlchemy

 -- Ondřej Nový <onovy@debian.org>  Fri, 20 Jan 2017 13:02:01 +0100

senlin (2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2016 18:24:15 +0200

senlin (2.0.0~rc1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Use correct branch in Vcs-* fields

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Build-Depends on openstack-pkg-tools >= 53~.
  * Fixed oslotest EPOCH.
  * Fixed oslo-config-generator namespaces.
  * Debconf translation:
    - it (Closes: #839079).

 -- Ondřej Nový <onovy@debian.org>  Mon, 26 Sep 2016 19:17:55 +0200

senlin (2.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using OpenStack's Gerrit as VCS URLs.
  * Fixed missing EPOC in oslo.config (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Sep 2016 14:35:46 +0200

senlin (2.0.0~b2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Updated Danish translation of the debconf templates (Closes: #830651).
  * Blacklist failing tests:
    - test_common_context.TestRequestContext.test_request_context_from_dict
    - test_common_context.TestRequestContext.test_request_context_init
    - test_common_context.TestRequestContext.test_request_context_update

 -- Thomas Goirand <zigo@debian.org>  Mon, 11 Jul 2016 14:37:23 +0200

senlin (2.0.0~b1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fix OS_TEST_PATH=./senlin/tests/unit when running unit tests.

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 Jun 2016 15:36:59 +0200

senlin (1.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * Updated Japanese debconf templates (Closes: #820758).
  * Updated Dutch debconf templates (Closes: #823440).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 May 2016 10:45:45 +0200

senlin (1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Added git as build-depends-indep.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Apr 2016 22:07:06 +0200

senlin (1.0.0~rc2-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2016 12:11:21 +0200

senlin (1.0.0~rc1-3) experimental; urgency=medium

  * Wrong template names nova->senlin.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 14:04:19 +0000

senlin (1.0.0~rc1-2) experimental; urgency=medium

  * Do not use Keystone admin auth token to register API endpoints.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 13:56:59 +0000

senlin (1.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Mar 2016 15:58:34 +0100

senlin (1.0.0~b3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Mar 2016 15:55:16 +0100

senlin (1.0.0~b2-1) experimental; urgency=medium

  * Initial release. (Closes: #812414)

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Jan 2016 19:33:45 +0800
